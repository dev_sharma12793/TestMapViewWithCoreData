//
//  CustCell.swift
//  TestMapViewWithCoreData
//
//  Created by Devendra Sharma on 10/11/17.
//  Copyright © 2017 Innvonix Technologies PVT ltd. All rights reserved.
//

import UIKit

class CustCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

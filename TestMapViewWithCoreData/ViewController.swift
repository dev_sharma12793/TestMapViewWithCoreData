//
//  ViewController.swift
//  TestMapViewWithCoreData
//
//  Created by Devendra Sharma on 10/11/17.
//  Copyright © 2017 Innvonix Technologies PVT ltd. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController,CLLocationManagerDelegate {
   
    var DetailData: [NSManagedObject] = []
    var startLocation: CLLocation!
    var lastLocation: CLLocation!

    var middleLocation: CLLocation!

    var latitude = String()
    var longitude = String()
    var annotation = MKPointAnnotation()
    var traveledDistance: Double = 0

    var locationManager: CLLocationManager = CLLocationManager()
    

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self

        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        startLocation = nil
        middleLocation = nil
    }
   
    func scheduleNotifications() {
        
        let content = UNMutableNotificationContent()
        let requestIdentifier = "Distance Reminder"
        
        content.title = "Distance Reminder"
        content.body = "Just a reminder that you walked 50 meter"
        content.categoryIdentifier = "actionCategory"
        content.sound = UNNotificationSound.default()
        
        // If you want to attach any image to show in local notification
        let url = Bundle.main.url(forResource: "selected_location", withExtension: ".png")
        do {
            let attachment = try? UNNotificationAttachment(identifier: requestIdentifier, url: url!, options: nil)
            content.attachments = [attachment!]
        }
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.1, repeats: false)
        
        let request = UNNotificationRequest(identifier: requestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { (error:Error?) in
            
            if error != nil {
                print(error?.localizedDescription)
            }     
            print("Notification Register Success")
        }
    }

    func getLocationAlways(application: UIApplication)
    {
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    // MARK: - getLocationWhenInUse - -Location -
    func getLocationWhenInUse(application: UIApplication)
    {
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let latestLocation: AnyObject = locations[locations.count - 1]
        
        latitude = String(format: "%.4f",
                          latestLocation.coordinate.latitude)
        longitude = String(format: "%.4f",
                           latestLocation.coordinate.longitude)
       
        if startLocation == nil {
            startLocation = locations.first
            traveledDistance = 0.0
            

        } else if let location = locations.last {
            traveledDistance += lastLocation.distance(from: location)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.traveledDistance = traveledDistance
            let objClass = self.tabBarController?.viewControllers?[1] as! UINavigationController
            let objDistanceClass = objClass.viewControllers[0] as! DistanceView
            objDistanceClass.refreshlbl(sender: traveledDistance)
            print("Traveled Distance:",  traveledDistance)
            print("Straight Distance:", startLocation.distance(from: locations.last!))
        }
        lastLocation = locations.last

        var location = CLLocation(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude) //changed!!!
        var addressString : String = ""

        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
               
                if pm?.subLocality != nil {
                    addressString = addressString + (pm?.subLocality!)! + ", "
                }
                if pm?.thoroughfare != nil {
                    addressString = addressString + (pm?.thoroughfare!)! + ", "
                }
                if pm?.locality != nil {
                    addressString = addressString + (pm?.locality!)! + ", "
                }
                if pm?.country != nil {
                    addressString = addressString + (pm?.country!)! + ", "
                }
                if pm?.postalCode != nil {
                    addressString = addressString + (pm?.postalCode!)! + " "
                }
                
                
                print(addressString)
                let centerCoordinate = CLLocationCoordinate2D(latitude: latestLocation.coordinate.latitude, longitude:latestLocation.coordinate.longitude)
                self.annotation.coordinate = centerCoordinate
                
                self.annotation.title = addressString
                self.mapView.addAnnotation(self.annotation) //drops the pin
                
                let userLocation = locations[0]
                
                
                let coordinate = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,
                                                        longitude: userLocation.coordinate.longitude)
                
                let span = MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001)
                
                let region = MKCoordinateRegion(center: coordinate, span: span)
                
                self.mapView.setRegion(region, animated: true)
                
                
                if (self.DetailData.count != 0){
                
                    let lastData = self.DetailData[self.DetailData.count - 1]
                    let lastDistLat = lastData.value(forKey: "latitude") as! String
                    let lastDistLong = lastData.value(forKey: "longitude") as! String
                    
                    var location = CLLocation(latitude: Double(lastDistLat)!, longitude: Double(lastDistLong)!) //changed!!!
                    
                    let distanceInMeters = latestLocation.distance(from: location) // result is in meters
                    
                    if(distanceInMeters >= 50)
                    {
                        
                        let date = Date()
                        let styler = DateFormatter()
                        styler.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let lDString = styler.string(from: date)
                        
                        let stringDistance = String(distanceInMeters)
                        
                        
                        let latitude = String(format: "%.4f",
                                              latestLocation.coordinate.latitude)
                        let longitude = String(format: "%.4f",
                                               latestLocation.coordinate.longitude)
                        
                        self.save(date: lDString, dist: stringDistance,latitude:latitude,longitude: longitude)
                        
                        
                        let state = UIApplication.shared.applicationState
                        
                        if state == .background {
                            
                            let appdele = UIApplication.shared.delegate as! AppDelegate
                            appdele.scheduleNotification()
                            
                        }
                        else if state == .active {
                            
                            self.scheduleNotifications()
                        }
                        
                    }
                    else
                    {
                        // out of 1 mile
                    }
                    
                
                }else{
                
                    let date = Date()
                    let styler = DateFormatter()
                    styler.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    let lDString = styler.string(from: date)
                    
                    let stringDistance = String(0)
                    
                    
                    let latitude = String(format: "%.4f",
                                          latestLocation.coordinate.latitude)
                    let longitude = String(format: "%.4f",
                                           latestLocation.coordinate.longitude)
                    
                    self.save(date: lDString, dist: stringDistance,latitude:latitude,longitude: longitude)

                }
               
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DetailData")
        do {
            DetailData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    func save(date: String,dist: String,latitude  :String, longitude :String) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "DetailData",
                                       in: managedContext)!
        
        let data = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        // 3
        data.setValue(date, forKeyPath: "date")
        data.setValue(dist, forKeyPath: "distance")
        data.setValue(latitude, forKeyPath: "latitude")
        data.setValue(longitude, forKeyPath: "longitude")


        // 4
        do {
            try managedContext.save()
            DetailData.append(data)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }


}
extension ViewController:UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert,.badge])
    }
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "action1":
            print("Action First Tapped")
        case "action2":
            print("Action Second Tapped")
        default:
            break
        }
        completionHandler()
    }
    
}


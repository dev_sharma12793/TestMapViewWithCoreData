//
//  AppDelegate.swift
//  TestMapViewWithCoreData
//
//  Created by Devendra Sharma on 10/11/17.
//  Copyright © 2017 Innvonix Technologies PVT ltd. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var traveledDistance: Double = 0
    var DetailData: [NSManagedObject] = []
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    
    var middleLocation: CLLocation!
    
    var latitude = String()
    var longitude = String()
    var annotation = MKPointAnnotation()
  
    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager: CLLocationManager = CLLocationManager()
    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted {
                print("Notification access denied.")
            }
        }

        registerForRichNotifications()

        return true
    }
    func registerForRichNotifications() {
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound]) { (granted:Bool, error:Error?) in
            if error != nil {
                print(error?.localizedDescription)
            }
            if granted {
                print("Permission granted")
            } else {
                print("Permission not granted")
            }
        }
        
        //actions defination
        let action1 = UNNotificationAction(identifier: "action1", title: "Action First", options: [.foreground])
        let action2 = UNNotificationAction(identifier: "action2", title: "Action Second", options: [.foreground])
        
        let category = UNNotificationCategory(identifier: "actionCategory", actions: [action1,action2], intentIdentifiers: [], options: [])
        
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func scheduleNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Distance Reminder"
        content.body = "Just a reminder that you walked 50 meter"
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: nil)
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
    }
    func getLocationAlways(application: UIApplication)
    {
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    // MARK: - getLocationWhenInUse - -Location -
    func getLocationWhenInUse(application: UIApplication)
    {
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        startLocation = nil
        middleLocation = nil

       locationManager.allowsBackgroundLocationUpdates = true
    locationManager.startMonitoringSignificantLocationChanges()
        //locationManager.startLocationUpdates()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let latestLocation: AnyObject = locations[locations.count - 1]
        
        latitude = String(format: "%.4f",
                          latestLocation.coordinate.latitude)
        longitude = String(format: "%.4f",
                           latestLocation.coordinate.longitude)
        
        if startLocation == nil {
            startLocation = locations.first
            traveledDistance = 0.0
            
        } else if let location = locations.last {
            traveledDistance += lastLocation.distance(from: location)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.traveledDistance = traveledDistance
            print("Traveled Distance:",  traveledDistance)
            print("Straight Distance:", startLocation.distance(from: locations.last!))
        }
        lastLocation = locations.last
        
        var location = CLLocation(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude) //changed!!!

        
                
                if (self.DetailData.count != 0){
                    
                    let lastData = self.DetailData[self.DetailData.count - 1]
                    let lastDistLat = lastData.value(forKey: "latitude") as! String
                    let lastDistLong = lastData.value(forKey: "longitude") as! String
                    
                    var location = CLLocation(latitude: Double(lastDistLat)!, longitude: Double(lastDistLong)!) //changed!!!
                    
                    let distanceInMeters = latestLocation.distance(from: location) // result is in meters
                    
                    if(distanceInMeters >= 50)
                    {
                        
                        let date = Date()
                        let styler = DateFormatter()
                        styler.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        let lDString = styler.string(from: date)
                        
                        let stringDistance = String(distanceInMeters)
                        
                        
                        let latitude = String(format: "%.4f",
                                              latestLocation.coordinate.latitude)
                        let longitude = String(format: "%.4f",
                                               latestLocation.coordinate.longitude)
                        
                        self.save(date: lDString, dist: stringDistance,latitude:latitude,longitude: longitude)
                        
                            self.scheduleNotification()
                        
                    }
                    else
                    {
                        // out of 1 mile
                    }
                    
                    
                }else{
                    
                    let date = Date()
                    let styler = DateFormatter()
                    styler.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    let lDString = styler.string(from: date)
                    
                    let stringDistance = String(0)
                    
                    
                    let latitude = String(format: "%.4f",
                                          latestLocation.coordinate.latitude)
                    let longitude = String(format: "%.4f",
                                           latestLocation.coordinate.longitude)
                    
                    self.save(date: lDString, dist: stringDistance,latitude:latitude,longitude: longitude)
                    
                }
                
                
                
                
                
                //   let distanceBetween: CLLocationDistance =
                //     latestLocation.distance(from: self.startLocation)
                
                //    let dist  = String(format: "%.2f", distanceBetween)
                
                
                
                
            
        
    }
    
    func save(date: String,dist: String,latitude  :String, longitude :String) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        // 1
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "DetailData",
                                       in: managedContext)!
        
        let data = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
        
        // 3
        data.setValue(date, forKeyPath: "date")
        data.setValue(dist, forKeyPath: "distance")
        data.setValue(latitude, forKeyPath: "latitude")
        data.setValue(longitude, forKeyPath: "longitude")
        
        
        // 4
        do {
            try managedContext.save()
            DetailData.append(data)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    

    func applicationWillEnterForeground(_ application: UIApplication) {
        locationManager.stopUpdatingLocation()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "TestMapViewWithCoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}


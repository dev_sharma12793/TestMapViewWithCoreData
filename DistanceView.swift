//
//  DistanceView.swift
//  TestMapViewWithCoreData
//
//  Created by Devendra Sharma on 11/11/17.
//  Copyright © 2017 Innvonix Technologies PVT ltd. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation


class DistanceView: UIViewController,CLLocationManagerDelegate {
   
    var DetailData : [NSManagedObject] = []
    var traveledDistance: Double = 0

    func refreshlbl(sender: Double) {
        if(distanceLabl == nil){
        }else{
            distanceLabl.text = String(format: "%.2f",
                                       sender) + " " + "Mtrs"
        }

    }

    @IBOutlet weak var distanceLabl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: Selector(("refreshlbl:")), name: NSNotification.Name(rawValue: "refresh"), object: nil)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        traveledDistance = appDelegate.traveledDistance
                        distanceLabl.text = String(format: "%.2f",
                                                   traveledDistance) + " " + "Mtrs"

        
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//            return
//        }
//        
//        let managedContext = appDelegate.persistentContainer.viewContext
//        
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DetailData")
//        do {
//            DetailData = try managedContext.fetch(fetchRequest)
//            
//            if(DetailData.count != 0){
//                let dataLast = DetailData[DetailData.count - 1]
//                let lastDistLat = dataLast.value(forKey: "latitude") as! String
//                let lastDistLong = dataLast.value(forKey: "longitude") as! String
//                
//                let locationLast = CLLocation(latitude: Double(lastDistLat)!, longitude: Double(lastDistLong)!) //changed!!!
//                
//                let dataFirst = DetailData[0]
//                let FirstDistLat = dataFirst.value(forKey: "latitude") as! String
//                let FirstDistLong = dataFirst.value(forKey: "longitude") as! String
//                
//                let locationfirst = CLLocation(latitude: Double(FirstDistLat)!, longitude: Double(FirstDistLong)!) //changed!!!
//                
//                let distanceInMeters = locationLast.distance(from: locationfirst)
//               //  = String(distanceInMeters)
//                distanceLabl.text = String(format: "%.2f",
//                                           distanceInMeters) + " " + "Mtrs"
//
//
//            }else{
//            
//            }
//            // result is in meters
//        } catch let error as NSError {
//            print("Could not fetch. \(error), \(error.userInfo)")
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

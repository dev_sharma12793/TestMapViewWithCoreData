//
//  DetailData+CoreDataProperties.swift
//  TestMapViewWithCoreData
//
//  Created by Devendra Sharma on 10/11/17.
//  Copyright © 2017 Innvonix Technologies PVT ltd. All rights reserved.
//

import Foundation
import CoreData


extension DetailData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DetailData> {
        return NSFetchRequest<DetailData>(entityName: "DetailData")
    }

    @NSManaged public var date: String?
    @NSManaged public var distance: String?
    @NSManaged public var latitude: String?
    @NSManaged public var longitude: String?

    @NSManaged public var id_Distance: Int16

}
